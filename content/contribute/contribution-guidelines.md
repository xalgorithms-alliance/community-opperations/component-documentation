---
title: 'Contribution Guidelines'
---

#### Quick Links

- figma
- etherpad 
- tweet drafts

#### Communications Brief

Anticipating the Digital assets conference, we plan to launch the new website before the 13th of September. This document outlines what that sprint looks like. It defines the strategy involved in this undertaking and how this translates into specific objectives. While this is intended to guide a relatively short sprint, these ideas are intended to help inaugurate a new chapter of the Xalgorithms Foundation and guide the high level strategy of work that comes after. 

#### Methodology 

Two main forces guide the development of this communications sprint.

1. Listening Tour 
2. User Interviews

From the information gathered from these methods, I articulate a communications strategy. A strategy, and a super structure of guiding metrics have largely been absent in the past. These will help ensure efficient use of design time, and effective results.  


#### Listening Tour

Coming back after taking a month off, I had directed conversations with a number of people including Joseph, Craig and Daniel. From this, a number of important elements emerged. 

##### Transformation 

 Xalgorithms is entering a new chapter. Oughtomation is more clearly defined than ever before. Don is working on building fundamental elements of this infrastructure. RM is downloadable. Chile is putting in Money. Bill is doubling down on his commitment. New people are joining the team and we are receiving greater visibility than ever before. 

Within the organization I believe that this is felt, though not stated explicitly. I see expanded ambition, and belief in what is possible. This is evidenced by the move to attend the Digital Asset Summit.

##### Expanding Organizational Capacity

This ongoing sprint has been oriented towards the Digital Asset Summit with the explicit goal of winning funding. However, winning funding is only one component of a larger initiative:

Expand Organizational capacity through 
	Winning funding
	Expanding Contributing community
	Filling competency gaps
	Building out the board and external advisors to improve decision making and strategy 

##### Directed Messaging

It can be confusing to switch between usecases and Oughtomation as a general purpose system. From conversations it is clear that there must be distinctions, conceptual and otherwise to prevent overload.

##### Connecting Strategies across Mediums 

Connect people, their stories, interests and contributions on social media through a series of monthly posts. 

Highlight the fact that as an organization and individuals we are always learning. Think of Xalgorithms as an invisible college. Have regular post sharing what we are reading/ what is influencing the thinking. 

#### User Interviews

This interview was originally intended to be focus on Rule Maker. However, given the course of the conversation, it became clear that the questions needed to be focused on the website and organization instead. 

The interviewee was a particuallry valuable source of feedback. She's the founder of a tech org who had not talked to anyone from the foundations previously. Her only contact had been through the paper and the website, making her both the kind of person the website should successfully engage, as well as coming in without any previous knowledge. 

Over the course of that conversation a few key ideas stand out. 

##### Advertise Progress

"not clear that this is something beyond an idea"

"didn't know there was an application being built"

##### Barriers to entry

"huge jargon barrier"

"I don't know if I'm welcome"

##### Confusion

"drink your own cool aid" in reference to simplicity

#### Strategy 

##### Connect to Organizational Strategy

expand focus to include all elements of organizational capacity building:
	Winning funding
	Expanding Contributing community
	Filling competency gaps
	Building out the board and external advisors to improve decision making and strategy 
	
These areas should guide the development of any additional content, and structure the calls to action/flows of the site. Fortunately, this is a simple shift within the context of the ongoing design sprint. With a renewed focus on the website, the above list can be utilized as a guiding strategic metric. In the same way that we are optimizing the website for the specific action of winning funds, the same process can be extended to the other areas of capacity building. As many of these are complimentary, this does greatly expand the scope of work, but rather offers a level of sensitivity to guide the redesign process. 

##### Create With Transformation in Mind

##### Personas

To help guide the process, personas are a useful exercise to contextualize the design challenges. The following personas are derived from the old web copy

-   Implementers
	-   organizations who bridge the gap between general purpose software and specific usecases. 
	-   there is an element of platform integration—inclusion within a product, application
	-   who: a founder, person with similar/ overlapping interests. probably a small start up or independent tech org
-   Developers & Researchers
	-   largely independent, though perhaps associated with an org,
	-   probably from a university, must be legible, would be good to highlight these collaborations 
-   R&D Donors
	-   donnors contributing funds
	-   gov, business, private entities
-   External Advisors / Board Members
	-   not sure about the difference between them and board members
	-   how to highlight their involvement?
	-   people who are ceos, have sway, important 
	-   interested in joining because they can see the potential of this work
	-   learned about the research or other endeavors, 

##### assessment methods

- use personas to do a heuristic markup 
	- find the things the different people would need to find
- validate flows from conversation through to web actions 

#### Steps Forward

##### Websites

There are two website that need to be ready for the conference
- Oughtomation 
- ERA

This is evidenced by the need to distinguish Oughotmation from usecases. 

##### Oughtomation

two pipelines to build out

- conceptual onboarding
- community onboarding 

what does this mean:

solutions:

- concepts page
- get involved page
- copy update

##### ERA

A simple single page site building anticipation. 

##### Questions to answer

what we do
where we're at
what you can do


##### Social Media

##### Email List
