---
title: 'UI Specifications'
---

<h3 id="specification">Application Specifications</h3>

Specification |  Quick Link
-- | --
Table Editor | [read](/xrm-dev/table-editor-ui)
Rule Templates |  [read](/xrm-dev/schema-arguments)
Rule Testing |  [read](/xrm-dev/rule-testing)

#### A Core Reference Implementation

The [Oughtomation Paper](https://gitlab.com/xalgorithms-alliance/oughtomation-paper) by Joseph Potvin et al specifies an:

_"end-to-end design for human-centred digitally-assisted project management with a general-purpose request-response method, [by which], anyone will be able to publish, discover, fetch, scrutinize, prioritize and have the capability to mobilize normative data on digital networks with precision, simplicity, scale, volume and speed, and with deference to prerogatives, agreements and preferences."_

This application is one of three core reference implementations that comprise the Oughtomation Method. Rule maker(RM) is responsible for the the description, maintenance, assembly, and publication of normative data within the Oughtomation specification. To this end, Rule Maker is concerned with "the imperative prerogative arising from social or institutional spheres which empowers a person in the ‘rule maker’ role to compose statutes, contracts, specifications, policies, treaties and other relational structures among two or more parties, " Potvin writes.  Crucially, this indicates that RM plays an interfacial role, at the edge of the network, facilitating the description of institutional, social and ecological prerogative into normative data that may be transmitted across the network. This is accomplished through the authorship of logic tables, and the description of accompanying metadata. 

From this the core actions of the application can be derived. RM must facilitate the authorship of logic tables, the description of required metadata, and the publication of this data to digital networks. 

----

<h3 id="philosophy">Philosophy</h3>

The design of this application has been guided by the both the technical specification of the Oughtomation paper, but also the design virtues that informed the research methodology of this document. In addition to these design virtues, the philosophy of general purpose software, and two core product design principles guided this project in ways that compliment and make specific the core design virtues. 

#### General Purpose Software

RM is general purpose software. This poses design challenges because the full range of use cases is not known. However, this imposes important constraints that prioritize flexibility. Throughout this applications are choices that were made to embrace flexibility. Understanding these choices is critical, because they impose certain technical requirements. Primarily:

1. The Rule Assembly panel is derived from rule templates that can describe the label, hint, input type, and input options for each field. This allows the interface to be customized to the specific needs of individual nodes. [See Schema Arguments for full discussion of this feature]().
2. Custom Logic State Icons can be used in the Logic Table, ensuring that a wide range of meaning can be expressed using this form. [See custom logic state icons form full description of this feature]().
3. Interface preferences that allow users to alter the interface to meet their screen preferences.

Beyond these three examples, the framework of general purpose software is a core concept that has informed the entire design process of this application. See [accessibility]() for more information about how these ideas inform the visual design of this applications. 

####  Design Virtues

there are guiding design principals from the forthcoming Oughtomation paper that can provide a grounding for the development of a brand. The following 4 design virtues became key points of orientation.

1. **Tolerance** The Oughtomation Paper describes the concept as follows: "Tolerance encompasses a designer’s spirit of respect for the prerogatives of those who are users of a design, or who are subject to its result, but also those across the community of other designers who would engage with the designed work in their preferred ways, for their own purposes, within their chosen domains, using their preferred technologies, and interpreted in the contexts of their respective normative paradigms." This has interesting implications for a brand, which normally known through rigid controlled. Opting for tolerance offers 
2. **Intuitiveness** This concept is best described by Christopher Alexander. The most enduring designs  are "so ordinary, that they strike to the core." Despite their ordinary nature, design that operates in this way is challenging to approach. As Alexander notes, "what makes them hard to find is not that they are unusual, strange or hard to express―but on the contrary that they are so ordinary.”(Alexander, 1979, p. 219)
3. **Least Power** This principal is most often employed in the context of programming languages. “Strength is a weakness when it comes to programming languages. The stronger and more expressive a programming language is, the more complex its code becomes. [...] Complex programs are more difficult to reason about and harder to identify edge cases for. [...] The less the language lets you do, the easier it is to analyze and prove properties." However this can also be applied to visual language."The less information a semantic path contains, the easier people understand and remember." 
4.  **Simplicity** The Oughtomation Paper cites how Saint-Exupéry praises the way  "the human eye will follow with effortless delight [..] a line that had not been invented but simply discovered, had in the beginning been hidden by nature and in the end had been found by the engineer." A similarly intuitive form is sought here.

#### Principles of Product Design
 
Complimenting and making specific the design virtues are are two core principles of product design that guide thinking within this application. These principles are focused on usability  

Having functional controls located correctly for application use is one of the most import elements of a functional UI. Two primary principles will guide this effort:
- Minimize complexity
- Reduce visual noise

Throughout the following sections these ideas will guide the thinking that supports the design decision of this application's UI. These principles both compliment and stem from 

-----
<h3 id="dev">XRM dev vs XRM</h3>

#### A productive point of tension

Ryan has created an electron desktop app. Joseph wants a webapp. However, there are reasons for Ryan's choice. This point of tension is a pivot point for an important discussion that will impact the timeline of RM, but will also help to clarify what makes RM Dev a dev tool, how it differs from an RM instance, and will resolve key work flow questions. 

Workflow is a key word here. In fact exploring the full work flow of a hypothetical author is how I arrived at The crux of this issue, and began to understand this point as crucial point of departure. 

Until now, the majority of UX thinking has revolved around rule assembly. This makes sense. This process involves new mechanics, complex interaction problems, and is the most obvious initial problem area. However, this focus has placed blinders up around the following elements of data flow. The are circled in yellow is the domain of this problem space.

Let's use a story to help frame the issue. 

*Ada has a sentence from a contract that she needs to describe using Rule Maker. She's not sure exactly how she's going to express it at the start, but jumps into the task. Filling out the logic table she is able to express her contract after a few tries. *

Amazing. All is going well. Now what?

##### One Simple Push 

*Now that Ada has made progress she would like to save her files, and commit them to a remote repository using GIT.* 

This action presents a number of crucial questions. The most fundamental is:

**How does she access git tooling?**

There are two options:
1. from the application
2. from the command line

This raises some problems. If git is accessed through the application, then how much control is given. How do we deal with conflicts, etc. This is a nightmare. With this route, we've inadvertently taken on the fools errand of building a GIT GUI. Not to mention this path would require deep API integration, meaning hours of work.

But if she accesses GIT tooling from the command line, that raises issues of access. Is this truly a no-code solution if a person needs to use the command line? Can domain experts really be expected to contribute if they require substantial outside technical know-how?

This may seem like an impasse, an irresolvable conflict. however, this is an opportunity, and our first clue on the path to understanding the distinction between a RM and an RM Dev.  

##### Publishing to RR

*With Ada's work stored in a remote repository, she is now ready to push her rules to RR.*

How does she do this?

This is not necessarily a difficult problem to solve in the UI. It could likely be accomplished with a target field and an upload button. However, it's unclear if more tools are required. There may be additional parameters that prove important to be able to specify. However, with a UI first approach, this can be difficult to assess. 

More importantly, all the following actions in the data flow are, as of yet, unconsidered. We know there will be incoming data about the rule life cycle, but the specifics are too murky to pursue much further.

So we will end the narrative as follows:

*Ada wants to access rule life cycle data using tooling as soon as its developed.*


#### Stop and think for a moment about how you would like to resolve these issues

- web app vs desktop app
- accessing git tooling
- RR integration 
- Resource Constraints

##### Possible options

**An all in one Application**

fork an AGPL text editor and bring in the work ryan has done so that it renders rule.json files, while also retaining text editing, pdf/jpg rendering, and command line capabilities. 

**Frankenstien's Monster**

multiple windows to achieve this
maybe xrm dev is even simpler, just a text editor with a few special files

**Web App**

Build a web app with git integration intended for non-technical users to simply author rules.

**Hybrid**

Bare bones web app intended to be used from localhost that only provides authoring interface with command line tooling accessible for devs. Intended as a sand box tool, as well as a template for people to build more complex webapps on top of. 

##### Realizing an IDE And Defining the Distinctions between an RM and RM Dev

The answer was in front of us the whole time.XRM Dev is literally a text editor. This is what Don has been saying the whole time. But 

RM Dev is a cloneable directory that a developer can interact with using their text editor of choice and a few command line tools
1. GIT
2. RR command line tooling allowing the user to push to RR, as well as pull rule life cycle data.
3. Rule testing from the command line*
4. script to launch an instance of an RR webapp that can be used to write `rule.xa` files as well as preview how changes to `rule-template.json` files are evaluated by the GUI 

* I now understand what testing from the command line looks like. And I think this is the most advantageous approach. Before I was imagining a GUI from which the author could produce sample `is.xalgo messages`. This would then return a response to the command line. This approach requires context switching and is awkward. Additionally, I believe that for many instances of a rule testing GUI, the user will want to be testing a rule in a familiar environment such as their own checkout flow. Any visual representation of testing in RM should be as customizable as possible to allow devs to create their own testing environments. 

RM is the web application running in the browser that can be used to write `rule.xa`. files. Git capabilities and RR integration will vary depending upon the instance and the work done by developers to add these features. 

This approach brings us back to the roots. When first getting involved with the Xalgorithms Foundation there was originally a lot of hesitation surrounding the creation of an RM. For good reason. People recognized the magnitude of such an undertaking. I believe that both Don and Ryan foresaw many of the roadblocks I've outlined here, and that Ryan's decision to create a desktop application was a result of this intuition.

Importantly, this approach also brings us closer to the target of a rule authoring IDE. Opening this up to the command line gives a number of key advantages. Full git tooling, ability to develop RR tooling, and, importantly, the ability to build out rule testing from the command line. 

The key thing is, this is a tool that can provide technical tooling for both rule handling, as well as for building custom instances of RM. It is a multi-function IDE.

This means we can focus on the back end creating capabilities that can be utilized by devs before we have integrated them into a GUI.

This also means more complex actions can be  taken up by other parties.  By providing a staging ground for more ambitious UIs. This will help us to harness the commercial impulses driving many possible devs. Someone can use the base work to create an elegant collaborative application for non-technical teams to collaborate on. Or someone could choose to develop deep git integration into a web app. The list goes on.



<h3 id="composition">IDE Environment</h3>

#### Rule Assembly Workflow

##### Rules Emerge from Human Relationships

In the Oughtomation paper, a rule is defined within specific terms. From figure 2 an important conceptual distinction is described. 

__Figure 2: Six types of normative data which may be communicated.__     

NORMATIVE DATA | DECISION | PROPOSITION | PREROGATIVE
-- | -- | -- | --
MUST, MAY and SHOULD | Empirical Statements | Declarative Statements | Imperative Statements
GENERAL CONTEXT |  Deduce from evidence that this system of rules is ‘in effect” for this jurisdiction and time. | Describe a system of rules.  | Acknowledge a system of rules.
PARTICULAR CIRCUMSTANCE | Deduce from evidence that this rule is ‘applicable’ to these facts. |  Describe a rule. | Acknowledge a rule.

From this it is clear that a rule that enters an Oughtomation specification network is a description of a rule based upon "some type of agreement, authority, resolve or preference." To this end an author using RM is tasked with describing an agreement that exists within a complex, dynamic rule system, emerging from living human relationships. These agreements are often described in formal legal documents, but may be documented in other ways such as meeting notes, a transcript of a conversation, a photograph, a note scribbled on a napkin, or any number of other methods. 

No matter the form of the document, the core authorship mechanic remains constant. An author, examines a document and describes a rule as normative data using a logic table. It is clear that before a rule exists within an Oughtomation network, it first exist in a form that is embedded within a human readable artifact. 

This is further evidenced by the fact that one of the core pieces of metadata in the rule schema (XRS) document is a `rule-url` field. This field is intended to ensure there is always reference to the rule's originating document. However, this choice also provides an important insight into the mechanics of an authorship process.

When an author decides to describe a complex and animate rule system based on living relationships, they may not know things such as:
- how many `rule.xalgo` documents will be required to fully express this rule
- how this description will be best be represented as a logic table 
- how many structured natural language sentences will be required
- what data tables will be required for rule evaluation

##### Dynamic IDE Assembly

To this end, an authorship process begins with examination of a source document and rule(s) are derived from careful and concerted description efforts. Annotation, isolation of clauses, etc. are all valuable tools that will aid the author in this effort. More important to the core mechanics of XRM Dev, an author must be able to create rules, and edit logic table __while__ examining a source document.

Understanding this dynamic give important clues to the nature of the IDE environment. Crucially, an author must be able to view more than one `rule.xalgo` document at once. They must be able to use the multi-plane environment to view a single source document while switching between the `assembly`  and `testing` panels of multiple rule documents. 

Coming to this realization makes me think back to a piece of advice Don Kelly has repeatedly given: "RM is a text editor". This comment has taken on further meaning. RM is a text editor, and therefore, it should be able to open a text document. However given the limited dev capacity, this is not the primary objective. The focus of the reference implementation must be on the assembly of logic tables and description of metadata. However recognizing that these element are _derived_ from a document provides important insights about how top level controls such as creating a new document, and opening existing documents are handled.

In summary, it will eventually be useful to conceptualize RM as a specific kind of  _note taking_ application. However, the ability to view source documents must not be the priority at this point. Yet even without this feature, conceptualizing the application with this workflow in mind will improve usability. Even if authors open a source document in another application, they will still be able to enjoy the kind of functionality they are looking for in a rule assemble environment, even if they lack the ability to view that document directly in rm application. 

This means a file browser to the left will be required. Within this module is a new rule button. This means that each pane need to display the file name and offer pane controls. See Split Pane for more details.

![multipane view of application](/screens/source-doc.png)

#### Tabs

The IDE environment is comprised of four tabs:

- Rule Assembly
	- Rule Metadata
	- [Logic Table](/xrm-dev/table-editor-ui)
- Raw Text
- Testing
- Settings

#### Layout

Concerted effort went into thinking about where each element sits within the window. Not only must the layout provide intuitive placement of the different elements, it must also do so effectively, even with reduced horizontal dimensions (since the split screen will limit the x dimension of the panel).

Layout Requirements  | --   |    --
-- | -- | --
have all the pages accessible via a submenu even when multiple panes are open | have all table controls accessible even when all panes are open | display maximum amount of table information given t once the other two requirements have been fulfilled 


because the application is multipane, it is imperative that controls and sub-navigation are accessible when even when half of the screen is occupied by another pane. This means that the sub-navigation must be located on the left hand side. There are too many tabs for it to be located below the navigation, since when the screen is split, tabs are lost. 

Similarly this puts a width constraint on the logic table control panel. Being located on the top means there is clear differentiation between the different submenus. Per application standards, these controls must consist of both an icon and a label, for maximum accessibility. However, this requires that the labels are as concise as possible, so that important control information is not hidden  It's width must be as small as possible so that controls are accessible even withe reduced x dimensions. 

![UI with areas of use highlighted](/screens/Frame-875.png)

#### Multipane

The author may split the screen horizontally to access multiple instances of the IDE interface.

----

<h3 id="vis">Visual Design</h3>

This application conforms to Xalgorithms Brand Standards. While not using the Xalgorithms Material Components, the atomic components comprising this application must be styled according to these specifications. 

See Brand Guidelines for full brand documentation

#### Brand Specifications

##### Color

##### Font

##### Border Radius 

#### Accessibility 
Throughout the application there a number of elements that must conform to standards. I will quickly layout a few universal rules that apply throughout the application
1. selection of an element must trigger a change in color and __At least__ one other UI element (ie. underline, outline, etc.).
2. Labels located with navigation and action control areas must consist of both Icon and text.
3. Hover states improve usability and are required for all clickable elements.

-----

<h3 id="beyond">Beyond</h3>

#### Collaboration

#### monitoring rule activity