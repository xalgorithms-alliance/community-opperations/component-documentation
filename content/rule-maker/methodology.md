---
title: 'Methodology'
order: 2
---

Designing a general purpose application is difficult. It requires holding an abstract view with enough distance to account for divergent and unexpected use cases. At the same time, it requires the same attention to the experiences of real people as any application with a specific metrics driven outcome.

With so many uses, a key challenge is to not be overly attached to any single use case. While some domain areas may present obvious solutions, these may not be applicable to other situations.