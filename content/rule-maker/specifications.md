---
title: 'Specifications'
order: 3
---

<h3 id="dev">XRM dev vs XRM</h3>

#### A productive point of tension

Ryan has created an electron desktop app. Joseph wants a webapp. However, there are reasons for Ryan's choice. This point of tension is a pivot point for an important discussion that will impact the timeline of RM, but will also help to clarify what makes RM Dev a dev tool, how it differs from an RM instance, and will resolve key work flow questions. 

Workflow is a key word here. In fact exploring the full work flow of a hypothetical author is how I arrived at The crux of this issue, and began to understand this point as crucial point of departure. 

Until now, the majority of UX thinking has revolved around rule assembly. This makes sense. This process involves new mechanics, complex interaction problems, and is the most obvious initial problem area. However, this focus has placed blinders up around the following elements of data flow. The are circled in yellow is the domain of this problem space.

Let's use a story to help frame the issue. 

*Ada has a sentence from a contract that she needs to describe using Rule Maker. She's not sure exactly how she's going to express it at the start, but jumps into the task. Filling out the logic table she is able to express her contract after a few tries. *

Amazing. All is going well. Now what?

##### One Simple Push 

*Now that Ada has made progress she would like to save her files, and commit them to a remote repository using GIT.* 

This action presents a number of crucial questions. The most fundamental is:

**How does she access git tooling?**

There are two options:
1. from the application
2. from the command line

This raises some problems. If git is accessed through the application, then how much control is given. How do we deal with conflicts, etc. This is a nightmare. With this route, we've inadvertently taken on the fools errand of building a GIT GUI. Not to mention this path would require deep API integration, meaning hours of work.

But if she accesses GIT tooling from the command line, that raises issues of access. Is this truly a no-code solution if a person needs to use the command line? Can domain experts really be expected to contribute if they require substantial outside technical know-how?

This may seem like an impasse, an irresolvable conflict. however, this is an opportunity, and our first clue on the path to understanding the distinction between a RM and an RM Dev.  

##### Publishing to RR

*With Ada's work stored in a remote repository, she is now ready to push her rules to RR.*

How does she do this?

This is not necessarily a difficult problem to solve in the UI. It could likely be accomplished with a target field and an upload button. However, it's unclear if more tools are required. There may be additional parameters that prove important to be able to specify. However, with a UI first approach, this can be difficult to assess. 

More importantly, all the following actions in the data flow are, as of yet, unconsidered. We know there will be incoming data about the rule life cycle, but the specifics are too murky to pursue much further.

So we will end the narrative as follows:

*Ada wants to access rule life cycle data using tooling as soon as its developed.*


#### Stop and think for a moment about how you would like to resolve these issues

- web app vs desktop app
- accessing git tooling
- RR integration 
- Resource Constraints

##### Possible options

**An all in one Application**

fork an AGPL text editor and bring in the work ryan has done so that it renders rule.json files, while also retaining text editing, pdf/jpg rendering, and command line capabilities. 

**Frankenstien's Monster**

multiple windows to achieve this
maybe xrm dev is even simpler, just a text editor with a few special files

**Web App**

Build a web app with git integration intended for non-technical users to simply author rules.

**Hybrid**

Bare bones web app intended to be used from localhost that only provides authoring interface with command line tooling accessible for devs. Intended as a sand box tool, as well as a template for people to build more complex webapps on top of. 

##### Realizing an IDE And Defining the Distinctions between an RM and RM Dev

The answer was in front of us the whole time.XRM Dev is literally a text editor. This is what Don has been saying the whole time. But 

RM Dev is a cloneable directory that a developer can interact with using their text editor of choice and a few command line tools
1. GIT
2. RR command line tooling allowing the user to push to RR, as well as pull rule life cycle data.
3. Rule testing from the command line*
4. script to launch an instance of an RR webapp that can be used to write `rule.xa` files as well as preview how changes to `rule-template.json` files are evaluated by the GUI 

* I now understand what testing from the command line looks like. And I think this is the most advantageous approach. Before I was imagining a GUI from which the author could produce sample `is.xalgo messages`. This would then return a response to the command line. This approach requires context switching and is awkward. Additionally, I believe that for many instances of a rule testing GUI, the user will want to be testing a rule in a familiar environment such as their own checkout flow. Any visual representation of testing in RM should be as customizable as possible to allow devs to create their own testing environments. 

RM is the web application running in the browser that can be used to write `rule.xa`. files. Git capabilities and RR integration will vary depending upon the instance and the work done by developers to add these features. 

This approach brings us back to the roots. When first getting involved with the Xalgorithms Foundation there was originally a lot of hesitation surrounding the creation of an RM. For good reason. People recognized the magnitude of such an undertaking. I believe that both Don and Ryan foresaw many of the roadblocks I've outlined here, and that Ryan's decision to create a desktop application was a result of this intuition.

Importantly, this approach also brings us closer to the target of a rule authoring IDE. Opening this up to the command line gives a number of key advantages. Full git tooling, ability to develop RR tooling, and, importantly, the ability to build out rule testing from the command line. 

The key thing is, this is a tool that can provide technical tooling for both rule handling, as well as for building custom instances of RM. It is a multi-function IDE.

This means we can focus on the back end creating capabilities that can be utilized by devs before we have integrated them into a GUI.

This also means more complex actions can be  taken up by other parties.  By providing a staging ground for more ambitious UIs. This will help us to harness the commercial impulses driving many possible devs. Someone can use the base work to create an elegant collaborative application for non-technical teams to collaborate on. Or someone could choose to develop deep git integration into a web app. The list goes on.