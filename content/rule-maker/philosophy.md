---
title: 'Philosophy'
order: 1
---

The design of this application has been guided by the both the technical specification of the Oughtomation paper, but also the design virtues that informed the research methodology of this document. In addition to these design virtues, the philosophy of general purpose software, and two core product design principles guided this project in ways that compliment and make specific the core design virtues. 

#### General Purpose Software

RM is general purpose software. This poses design challenges because the full range of use cases is not known. However, this imposes important constraints that prioritize flexibility. Throughout this applications are choices that were made to embrace flexibility. Understanding these choices is critical, because they impose certain technical requirements. Primarily:

1. The Rule Assembly panel is derived from rule templates that can describe the label, hint, input type, and input options for each field. This allows the interface to be customized to the specific needs of individual nodes. [See Schema Arguments for full discussion of this feature]().
2. Custom Logic State Icons can be used in the Logic Table, ensuring that a wide range of meaning can be expressed using this form. [See custom logic state icons form full description of this feature]().
3. Interface preferences that allow users to alter the interface to meet their screen preferences.

Beyond these three examples, the framework of general purpose software is a core concept that has informed the entire design process of this application. See [accessibility]() for more information about how these ideas inform the visual design of this applications. 

####  Design Virtues

there are guiding design principals from the forthcoming Oughtomation paper that can provide a grounding for the development of a brand. The following 4 design virtues became key points of orientation.

1. **Tolerance** The Oughtomation Paper describes the concept as follows: "Tolerance encompasses a designer’s spirit of respect for the prerogatives of those who are users of a design, or who are subject to its result, but also those across the community of other designers who would engage with the designed work in their preferred ways, for their own purposes, within their chosen domains, using their preferred technologies, and interpreted in the contexts of their respective normative paradigms." This has interesting implications for a brand, which normally known through rigid controlled. Opting for tolerance offers 
2. **Intuitiveness** This concept is best described by Christopher Alexander. The most enduring designs  are "so ordinary, that they strike to the core." Despite their ordinary nature, design that operates in this way is challenging to approach. As Alexander notes, "what makes them hard to find is not that they are unusual, strange or hard to express―but on the contrary that they are so ordinary.”(Alexander, 1979, p. 219)
3. **Least Power** This principal is most often employed in the context of programming languages. “Strength is a weakness when it comes to programming languages. The stronger and more expressive a programming language is, the more complex its code becomes. [...] Complex programs are more difficult to reason about and harder to identify edge cases for. [...] The less the language lets you do, the easier it is to analyze and prove properties." However this can also be applied to visual language."The less information a semantic path contains, the easier people understand and remember." 
4.  **Simplicity** The Oughtomation Paper cites how Saint-Exupéry praises the way  "the human eye will follow with effortless delight [..] a line that had not been invented but simply discovered, had in the beginning been hidden by nature and in the end had been found by the engineer." A similarly intuitive form is sought here.

#### Principles of Product Design
 
Complimenting and making specific the design virtues are are two core principles of product design that guide thinking within this application. These principles are focused on usability  

Having functional controls located correctly for application use is one of the most import elements of a functional UI. Two primary principles will guide this effort:
- Minimize complexity
- Reduce visual noise

Throughout the following sections these ideas will guide the thinking that supports the design decision of this application's UI. These principles both compliment and stem from 