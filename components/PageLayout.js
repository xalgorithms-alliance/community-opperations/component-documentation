import style from './PageLayout.module.css'
import XrmSidebar from './XrmSidebar'
import Footer from './Footer'


export default function PageLayout({children, posts, next, brand=[], writing=[], component=[], rulemaker=[], contribute=[]}) {
    return (
        <>
        <div className={style.hold}>
            <div>
                <div className={style.links}>
                    <XrmSidebar next={next} links={posts}/>
                </div>
            </div>
            <div>
                {children}
            </div>
        </div>
        <Footer brand={brand} writing={writing} component={component} rulemaker={rulemaker} contribute={contribute}/>
        </>
    )
}