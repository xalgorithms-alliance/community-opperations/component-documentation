import { getPostData, getSortedPostsData } from '../lib/components'
import { Column, Grid12, Button } from 'xf-material-components/package/index'
import XrmSidebar from '../components/XrmSidebar'
import Head from 'next/head'
import PageLayout from '../components/PageLayout'
import Subsection from '../components/Subsection'

export async function getStaticProps() {
  const postData = await getPostData('content/rule-maker', 'intro')
  const allPostsData = getSortedPostsData('content/rule-maker')
  return {
    props: {
      postData,
      allPostsData
    }
  }
}


  const content = {
    gridArea: '1/4/1/8',
    width: '550px',
    paddingTop: '169px'
  }

  const links = {
    position: '-webkit-sticky',
    position: 'sticky',
    top: '140px',
    height: '200px',
    gridArea: '1/2/1/4',
  }

  const spacer = {
    height: '400px',
  }

  

export default function xrmDev({postData, allPostsData }) {
  console.log(postData.next)
  
    const renderSection = () => {
      return(
        allPostsData.map(({title}, index) => (
          <Subsection title={title} key={index}/>
        ))
      )
    }

    return (
      <div>
        <Head>
          <title>XRM</title>
        </Head>

          <PageLayout posts={allPostsData} rulemaker={allPostsData}>
          <div className="texthold">
            <Column>
              <div className="SetWritingHold">
                <h4>XRM Dev UI Specifications</h4>
                <p>Contribute to the development of the XRM Dev interface. See the specification and the thinking that went into them.</p>
                <Button target="https://gitlab.com/xalgorithms-alliance/xalgo-devrm-v2">Project Repo</Button>
              </div>
              <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} className="SetWritingHold"/>
              <div style={spacer}/>
              {renderSection()}
            </Column>
          </div>
          </PageLayout>

        
      </div>
    )
  }