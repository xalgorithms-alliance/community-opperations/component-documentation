import { getSortedPostsData } from '../lib/components'
import { Column, Grid12, LinkButton } from 'xf-material-components/package/index'
import Header from '../components/Header'

export async function getStaticProps() {
    const allPostsData = getSortedPostsData('content/rule-maker')
    return {
      props: {
        allPostsData
      }
    }
  }

  const content = {
    gridArea: '1/4/1/8',
    width: '550px'
  }

  const links = {
    position: '-webkit-sticky',
    position: 'sticky',
    top: '140px',
    height: '200px',
    gridArea: '1/2/1/4'
  }

  const padding = {
    paddingTop: '1em'
  }

export default function xrmDev({allPostsData}) {


    return (
      <div>
        <h1>Hello World</h1>
        <div>
          {/*header image*/}
        </div>
        
        <h1>
          Welcome
        </h1>
        <div>
          <div>
            {/*image*/}
          </div>
          <div>
            <h3>
              Title
            </h3>
          </div>
        </div>

      </div>
    )
  }